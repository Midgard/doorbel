# This file is part of Doorbel, which is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# The software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU General Public License along with this software.
# If not, see <https://www.gnu.org/licenses/>.

import os
import falcon

from doorbel.config       import config as load_config
from doorbel.env          import env_var
from doorbel.template     import Templates
from doorbel.notification import dispatch_notification


API_SOURCE_CODE  = env_var("DBL_API_SOURCE_CODE",  default="https://framagit.org/Midgard/doorbel")
API_LICENSE_TEXT = env_var("DBL_API_LICENSE_TEXT", default="This service is powered by free software. The source code is available at " + API_SOURCE_CODE)

config = load_config()
templates = Templates(API_SOURCE_CODE, config["strings"], config["base_path"])


class HomeResource:
	def on_get(self, req: falcon.request.Request, resp: falcon.response.Response):
		resp.content_type = falcon.MEDIA_HTML
		resp.media = templates.html_home(config["users"])


class UserResource:
	def on_post(self, req, resp, user_id):
		try:
			user = config["users"][int(user_id)]
		except (KeyError, ValueError):
			raise falcon.HTTPNotFound(description="User not found")

		successes = [
			dispatch_notification(notification_def)
			for notification_def in user["notifications"]
		]

		if not any(successes):
			raise falcon.HTTPInternalServerError(description="Could not dispatch notification")

		resp.content_type = falcon.MEDIA_HTML
		resp.media = templates.html_called(user)


class SimpleHandler(falcon.media.BaseHandler):
	def serialize(self, media, content_type):
		return media.encode("utf-8")

	def deserialize(self, stream, content_type, content_length):
		raise NotImplementedError()


api = falcon.API()

extra_handlers = {
	falcon.MEDIA_HTML: SimpleHandler()
}
api.resp_options.media_handlers.update(extra_handlers)

api.add_route("/",          HomeResource())
api.add_route("/{user_id}", UserResource())
api.add_static_route("/static", os.path.join(os.path.dirname(__file__), "static"))
