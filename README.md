# Doorbel [<img src="/.repo/agplv3-155x51.png" width="77" height="25" alt="licensed under the AGPL"/>](/LICENSE)

Have your friends ring your bell over the internet. Useful if your bell is broken or you want to be
rung while you're not at home.

## Development

### Install

1. Install PyPy, or CPython 3.5.
2. Install [pipenv](https://pypi.org/project/pipenv/).
3. Clone this repository and cd into it.
4. Run `pipenv --python /usr/bin/pypy3` to set up the virtualenv through pipenv, using PyPy. Skip
   this step if you want to use CPython.
5. Run `pipenv install` in the working directory of this repo.
6. Run `./run-dev.sh` to run the site locally.

### Code style and editor

It's advisable to use an editor with [EditorConfig](https://editorconfig.org/) support to do
indentation the right way. Some editors ship with out-of-the-box support and some support it
through a plug-in.

We use tabs for indentation, disobeying PEP-8. If you have an EditorConfig-aware editor as
recommended above, you shouldn't need to worry about this.

## Deployment

See [deploy/README.md](./deploy/README.md).
