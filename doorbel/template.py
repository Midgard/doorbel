# This file is part of Doorbel, which is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# The software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU General Public License along with this software.
# If not, see <https://www.gnu.org/licenses/>.


class Templates:
	def __init__(self, API_SOURCE_CODE, strings, base_path):
		self.API_SOURCE_CODE = API_SOURCE_CODE
		self.strings = strings
		self.base_path = base_path.rstrip("/")

	def _html_page(self, title, content, classes=[]):
		return """<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>{pagetitle}</title>
		<link rel="stylesheet" href="{base_path}/static/main.css"/>
	</head>
	<body class="{classes}">
		<h1>{title}</h1>
		<main>
{content}
		</main>
		<footer>Powered by <a href="{API_SOURCE_CODE}">Doorbel</a></footer>
	</body>
</html>
""".format(
			classes=" ".join(classes),
			pagetitle=("{title} – {name}" if title else "{name}").format(
				title=title, name=self.strings["name"]),
			title=title if title else self.strings["name"],
			content=content,
			base_path=self.base_path,
			API_SOURCE_CODE=self.API_SOURCE_CODE
		)

	def html_home(self, users):
		user_buttons = "\n".join(
			'\t\t\t<li><form action="{base_path}/{user_id}" method="post">'
			'<input type="submit" value="{user_name}"/>'
			'</form></li>'.format(
				user_id=i, user_name=user["name"],
				base_path=self.base_path
			)
			for i, user in enumerate(users)
		)

		return self._html_page(
			title=None,
			content="\t\t<ul>\n{user_buttons}\t\t</ul>".format(user_buttons=user_buttons)
		)

	def html_called(self, user):
		return self._html_page(
			classes=["success"],
			title=self.strings["called"].format(user_name=user["name"]),
			content='\t\t<p><a href="{base_path}/">{back}</a></p>'.format(
				back=self.strings["back"], base_path=self.base_path)
		)
