# This file is part of Doorbel, which is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# The software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU General Public License along with this software.
# If not, see <https://www.gnu.org/licenses/>.

import requests


def mattermost(noti_def):
	assert "url" in noti_def

	data = {
		"text": "@all Ding-dong!"
	}
	if "text" in noti_def:
		data["text"] = noti_def["text"]
	if "channel" in noti_def:
		data["channel"] = noti_def["channel"]

	requests.post(noti_def["url"], json=data)


handlers = {
	"mattermost": mattermost
}


def dispatch_notification(noti_def):
	if noti_def["type"] in handlers:
		handlers[noti_def["type"]](noti_def)
		return True
	else:
		return False
